package org.lab;

import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.blocks.locking.LockService;
import org.jgroups.util.Rsp;
import org.jgroups.util.RspList;
import org.jgroups.util.Util;

import java.io.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

/**
 * Replicated stock server; every cluster node has the same state (stocks).
 */
public class ReplicatedStockServer extends ReceiverAdapter {
    private final Map<String, Double> stocks = new HashMap<>();
    private JChannel channel;
    private LockService lock_service;
    private RpcDispatcher disp; // to invoke RPCs

    private static class StockValue implements Serializable {
        double value;
        LocalDateTime set = LocalDateTime.now();

        public StockValue(double value) {
            this.value = value;
        }
    }


    /**
     * Assigns a value to a stock
     */
    public void _setStock(String name, Double value) {
        synchronized (stocks) {
            stocks.put(name, value);
            System.out.printf("-- set %s to %s\n", name, value);
        }
    }

    /**
     * Removes a stock from the hashmap
     */
    public void _removeStock(String name) {
        synchronized (stocks) {
            stocks.remove(name);
            System.out.printf("-- removed %s\n", name);
        }
    }

    /**
     * Get a stock from the hashmap
     */
    public Double _getStock(String name) {
        synchronized (stocks) {
            Double val = stocks.get(name);
            System.out.printf("-- returned %s with %s\n", name, val);
            return val;
        }
    }

    /**
     * Set a stock if it's value equal to ref
     */
    public boolean _casStock(String name, double reference, Double newValue) {
        synchronized (stocks) {
            Double val = stocks.get(name);
            if (val == reference) {
                stocks.put(name, newValue);
                System.out.printf("-- cas -- set %s to %s\n", name, newValue);
                return true;
            } else {
                System.out.printf("-- cas -- failed in setting %s to %s\n", name, newValue);
                return false;
            }
        }
    }

    private void start(String props) throws Exception {
        channel = new JChannel(props);
        disp = new RpcDispatcher(channel, this).setMembershipListener(this);
        lock_service = new LockService(channel);
        disp.setStateListener(this);
        channel.connect("stocks");
        disp.start();
        channel.getState(null, 30000); // fetches the state from the coordinator
        while (true) {
            int c = Util.keyPress("[1] Show stocks [2] Get quote [3] Set quote [4] Remove quote [5] CAS [x] Exit");
            try {
                switch (c) {
                    case '1':
                        showStocks();
                        break;
                    case '2':
                        getStock();
                        break;
                    case '3':
                        setStock();
                        break;
                    case '4':
                        removeStock();
                        break;
                    case '5':
                        casStock();
                        break;
                    case 'x':
                        channel.close();
                        return;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
    }


    public void viewAccepted(View view) {
        System.out.println("-- VIEW: " + view);
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        DataOutput out = new DataOutputStream(output);
        synchronized (stocks) {
            System.out.println("-- returning " + stocks.size() + " stocks");
            Util.objectToStream(stocks, out);
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public void setState(InputStream input) throws Exception {
        DataInput in = new DataInputStream(input);
        Map<String, Double> new_state = Util.objectFromStream(in);
        System.out.println("-- received state: " + new_state.size() + " stocks");
        synchronized (stocks) {
            stocks.clear();
            stocks.putAll(new_state);
        }
    }


    private void getStock() throws Exception {
        String ticker = readString("Symbol");
        Lock lock = lock_service.getLock("lock" + ticker);
        lock.lock();
        try {
            synchronized (stocks) {
                Double val = stocks.get(ticker);
                System.out.println(ticker + " is " + val);
            }
        } finally {
            lock.unlock();
        }
    }

    private boolean casStock() throws Exception {
        String ticker = readString("Symbol");
        double oldValue = Double.parseDouble(readString("Old value"));
        String val = readString("New value");

        Lock lock = lock_service.getLock("lock" + ticker);
        lock.lock();
        try {
            System.out.printf("Old value: %s", oldValue);

            RspList<Boolean> rsps = disp.callRemoteMethods(null, "_casStock",
                    new Object[]{ticker, oldValue, (Double.parseDouble(val))},
                    new Class[]{String.class, double.class, Double.class}, RequestOptions.SYNC());

            int countSucceeded = 0;

            for (Rsp<Boolean> v : rsps) {
                if (v.wasReceived() && v.getValue() == true) {
                    countSucceeded++;
                }
            }

            boolean succeeded = countSucceeded > rsps.numReceived() / 2;

            System.out.println("cas: " + ticker + " set to " + val + (succeeded ? " successfully" : " FAILED"));
            return succeeded;
        } finally {
            lock.unlock();
        }
    }

    private void setStock() throws Exception {
        String ticker, val;
        ticker = readString("Symbol");
        val = readString("Value");
        Lock lock = lock_service.getLock("lock" + ticker);
        lock.lock();
        try {
            RspList<Void> rsps = disp.callRemoteMethods(null, "_setStock", new Object[]{ticker, Double.parseDouble(val)},
                    new Class[]{String.class, Double.class}, RequestOptions.SYNC());
            System.out.println("rsps:\n" + rsps);
        } finally {
            lock.unlock();
        }
    }


    private void removeStock() throws Exception {
        String ticker = readString("Symbol");
        Lock lock = lock_service.getLock("lock" + ticker);
        lock.lock();
        try {
            RspList<Void> rsps = disp.callRemoteMethods(null, "_removeStock", new Object[]{ticker},
                    new Class[]{String.class}, RequestOptions.SYNC());
            System.out.println("rsps:\n" + rsps);
        } finally {
            lock.unlock();
        }
    }

    private void showStocks() {
        System.out.println("Stocks:");
        synchronized (stocks) {
            for (Map.Entry<String, Double> entry : stocks.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        }
    }


    private static String readString(String s) throws IOException {
        int c;
        boolean looping = true;
        StringBuilder sb = new StringBuilder();
        System.out.print(s + ": ");
        System.out.flush();
        System.in.skip(System.in.available());

        while (looping) {
            c = System.in.read();
            switch (c) {
                case -1:
                case '\n':
                case 13:
                    looping = false;
                    break;
                default:
                    sb.append((char) c);
                    break;
            }
        }

        return sb.toString();
    }


    public static void main(String[] args) throws Exception {
        String props = "udp.xml";
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-props")) {
                props = args[++i];
                continue;
            }
            System.out.println("ReplicatedStockServer [-props <XML config file>]");
            return;
        }

        new ReplicatedStockServer().start(props);
    }


}
